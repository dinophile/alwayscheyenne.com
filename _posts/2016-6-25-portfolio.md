---
title:  "Check out my Github!"
subtitle: "Be kind! I'm a work in progress!"
author: "Cheyenne"
avatar: "img/authors/cheyenne.png"
image: "img/e.jpg"
date:   2016-06-25 3:24 PM
---

### So...

My github is very very new and sparsely populated. It showcases my learning journey for the moment. I will be adding projects that I am working on to showcase my awesomeness!

## Feel free to comment!

I know that most of the repos are assignment code, and not always finished, but if you see anything you'd like to comment on or have something new for me to google please feel free to let me know!
