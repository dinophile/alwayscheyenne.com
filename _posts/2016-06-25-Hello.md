---
title:  "Welcome!"
subtitle: "All about me!"
author: "Cheyenne"
avatar: "img/authors/cheyenne.png"
image: "img/streetcar.jpg"
date:   2016-07-22 1:44 PM
---

### Welcome to my new path!

My name is Cheyenne and I have recently rediscovered coding after many years of dabbling. I've been playing around computers since the early 80s and I'm continuously amazed at what wonders this industry gives us! And now I'm a part of it all too!

### I may be new but...

I've got big ideas! I have recently completed a coding bootcamp with the amazing people at Bitmaker here in Toronto. Even though I've mainly learned that I have a lot more to learn, I've made some pretty interesting things and will continue to work on my projects to learn even more!

### I'm always interested in a chat!

If you're ever interested in teaching something cool you just learned about, I'm happy to listen! I want to expand my skills and confidence in order to work with great people and build even bigger and better things! Feel free to get in touch!
